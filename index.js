const express = require('express');
const app = express();

const cors = require('cors');
const axios = require('axios');

const parseString = require('xml2js').parseString;
const url = require('url');

app.use(cors());

const port = 1234;

app.get('/', (req, res) => {
  var url_parts = url.parse(req.url, false);
  var query = url_parts.query;
  axios.get(query)
  .then( response => {
    parseString(response.data, (err, result) => {
      res.json(result['dnsa:search-response']['dnsa:results'][0]);
      console.log(result);
    })
  });
})

app.listen(port, () => {
  console.log('App is alive on port 1234!');
})