var sax = require("sax");
var util = require('util');

module.exports = XMLParser;

/**
 * parses an xml input string and call listener for objects matching any provided tag name.
 * @param tags array of elements to match. each element is turned into an object to given to listener.
 * @constructor
 */
function XMLParser(tags)
{
    this.elements = tags;
    this.callbacks = {};
}

XMLParser.prototype.toObject = function (xml, cb)
{
    var result = [];
    this.onObject(function (tag, obj)
    {
        result.push({tag:tag, value:obj});
    });

    this.onError(function (e)
    {
        cb(e, null);
    });

    this.onEnd(function ()
    {
        cb(null, result);
    });

    try {
        this.parse(xml);
    }catch(e) {
        cb(e);
    }
};

/**
 * Register cb for objects
 * @param callback function(tagName, object)
 */
XMLParser.prototype.onObject = function (callback)
{
    this.callbacks['object'] = callback;
};

/**
 * Register cb for errors
 * @param callback function(err)
 */
XMLParser.prototype.onError = function (callback)
{
    this.callbacks['error'] = callback;
};

/**
 * Register cb for end of document
 * @param callback function()
 */
XMLParser.prototype.onEnd = function (callback)
{
    this.callbacks['end'] = callback;
};

XMLParser.prototype.parse = function (inputDataString)
{
    var self = this;

    var parser = sax.parser(true);

    var currentObject;
    var inObject = false;
    var inObjectName;
    var ancestors = [];

    parser.onopentag = function (args)
    {
        if (!inObject)
        {
            // If we are not in an object and not tracking the element
            // then we don't need to do anything
            if (self.elements.indexOf(args.name) < 0)
            {
                return;
            }

            // Start tracking a new object
            inObject = true;
            inObjectName = args.name;

            currentObject = {};
        }

        if (!(args.name in currentObject))
        {
            currentObject[args.name] = args.attributes;
        } else if (!util.isArray(currentObject[args.name]))
        {
            // Put the existing object in an array.
            var newArray = [currentObject[args.name]];

            // Add the new object to the array.
            newArray.push(args.attributes);

            // Point to the new array.
            currentObject[args.name] = newArray;
        } else
        {
            // An array already exists, push the attributes on to it.
            currentObject[args.name].push(args.attributes);
        }

        // Store the current (old) parent.
        ancestors.push(currentObject);

        // We are now working with this object, so it becomes the current parent.
        if (currentObject[args.name] instanceof Array)
        {
            // If it is an array, get the last element of the array.
            currentObject = currentObject[args.name][currentObject[args.name].length - 1];
        } else
        {
            // Otherwise, use the object itself.
            currentObject = currentObject[args.name];
        }
    };

    parser.ontext = function (data)
    {
        if (!inObject)
        {
            return;
        }

        data = data.trim();

        if (!data.length)
        {
            return;
        }

        currentObject['$t'] = (currentObject['$t'] || "") + data;
    };

    parser.onclosetag = function (name)
    {
        if (!inObject)
        {
            return;
        }

        if (inObject && inObjectName === name)
        {
            // Finished building the object
            emit('object', name, currentObject);

            inObject = false;
            ancestors = [];

            return;
        }

        if (ancestors.length)
        {
            var ancestor = ancestors.pop();
            var keys = Object.keys(currentObject);

            if (keys.length === 1 && '$t' in currentObject)
            {
                // Convert the text only objects into just the text
                if (ancestor[name] instanceof Array)
                {
                    ancestor[name].push(ancestor[name].pop()['$t']);
                } else
                {
                    ancestor[name] = currentObject['$t'];
                }
            } else if (!keys.length)
            {
                // Remove empty keys
                delete ancestor[name];
            }

            currentObject = ancestor;
        } else
        {
            currentObject = {};
        }
    };

// Rebroadcast the error and keep going
    parser.onerror = function (e)
    {
        throw e;
    };

// Rebroadcast the end of the file read
    parser.onend = function ()
    {
        emit("end");
    };


    parser.write(inputDataString).close();

    function emit(cmd, a1, a2)
    {
        var callback = self.callbacks[cmd];
        if (typeof callback === 'function')
        {
            callback(a1, a2);
        }
    }
};